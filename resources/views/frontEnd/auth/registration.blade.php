<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('/public/superadminPanel/')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{asset('/public/superadminPanel/')}}/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{asset('/public/superadminPanel/')}}/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{asset('/public/superadminPanel/')}}/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign Up</h3>
                    </div>
                    @if(Session::has('success'))
		                <p class="alert alert-success">{{ Session::get('success') }}</p>
		            @elseif(Session::has('danger'))
		                <p class="alert alert-danger">{{ Session::get('danger') }}</p>
		            @endif
                    <div class="panel-body">
                        <form role="form" action="{{url('/user/store')}}" method="post">
                        	{{csrf_field()}}
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="First Name" name="fName" type="text" autofocus>
                                    @if($errors->has('fName'))
                                        <span class="text-danger">{{ $errors->first('fName') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Last Name" name="lName" type="text" autofocus>
                                    @if($errors->has('lName'))
                                        <span class="text-danger">{{ $errors->first('lName') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Phone" name="phone" type="number" autofocus>
                                    @if($errors->has('phone'))
                                        <span class="text-danger">{{ $errors->first('phone') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Company Name" name="companyName" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="text">
                                    @if($errors->has('email'))
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password">
                                    @if($errors->has('password'))
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <span style="margin-left: 5px;"><input type="radio" name="userType" value="seller" style="margin-right: 5px;">I am a Seller</span>
                                    <span style="margin-left: 10px;"><input type="radio" name="userType" value="buyer" style="margin-right: 5px;">I am a Buyer</span>
                                    @if($errors->has('userType'))
                                        <span class="text-danger">{{ $errors->first('userType') }}</span>
                                    @endif
                                </div>
                                <!-- <div class="form-group">
                                	<select class="form-control" name="userType">
                                		<option value="">Select Type</option>
                                		<option value="seller">I am a Seller</option>
                                		<option value="buyer">I am a Buyer</option>
                                	</select>
                                	@if($errors->has('userType'))
                                        <span class="text-danger">{{ $errors->first('userType') }}</span>
                                    @endif
                                </div> -->
                                
                                <!-- Change this to a button or input when using this as a form -->
                                <button class="btn-lg btn-success btn-block">Sign Up</button>
                            </fieldset>
                        </form>
                        <a href="{{url('/login')}}" style="margin-left: 82px;">Already have an account.?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="{{asset('/public/superadminPanel/')}}/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('/public/superadminPanel/')}}/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{asset('/public/superadminPanel/')}}/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{asset('/public/superadminPanel/')}}/dist/js/sb-admin-2.js"></script>

</body>

</html>
