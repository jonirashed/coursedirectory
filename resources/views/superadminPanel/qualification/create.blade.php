 @extends('superadminPanel.master')
 
 @section('mainContent')
 <div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add New Qualification</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                            	@if(Session::has('success'))
					                <p class="alert alert-success">{{ Session::get('success') }}</p>
					            @elseif(Session::has('danger'))
					                <p class="alert alert-danger">{{ Session::get('danger') }}</p>
					            @endif
                                <div class="col-lg-8">
                                    <form role="form" action="{{url('superAdminBangla1desh/qualification')}}" method="post">
                                    	{{csrf_field()}}
                                    	<div class="form-group mrgn {{ $errors->has('name') ? 'has-error' : '' }}">
	                                    	<div class="col-sm-4">
		                                    	<label>Name</label>
		                                    </div>
		                                    <div class="col-sm-8 ">
		                                        <input type="text" name="name" class="form-control mrgn_btn" placeholder="Full Name" value="{{ old('name') }}">
		                                        @if($errors->has('name'))
		                                            <span class="text-danger">{{ $errors->first('name') }}</span>
		                                        @endif
		                                    </div>
		                                </div>
		                                <div class="form-group">
	                                    	<div class="col-sm-4">
		                                    	<label>Slug</label>
		                                    </div>
		                                    <div class="col-sm-8">
		                                        <input type="text" name="slug" class="form-control mrgn_btn" placeholder="Slug">
		                                    </div>
		                                </div>
                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                            <div class="col-sm-4">
		                                    	<label>Publication Status</label>
		                                    </div>
		                                    <div class="col-sm-8">
	                                            <select class="form-control mrgn_btn" name="status">
	                                                <option value="">Select Status</option>
	                                                <option value="1">Publish</option>
	                                                <option value="2">Unpublish</option>
	                                            </select>
	                                            @if($errors->has('status'))
		                                            <span class="text-danger">{{ $errors->first('status') }}</span>
		                                        @endif
	                                        </div>
                                        </div>
		                                <div class="form-group">
		                                    <div class="col-sm-4 pull-right">
		                                        <button type="submit" class="btn btn-primary">Create</button>
		                                        <button type="reset" class="btn btn-default">Reset Button</button>
		                                    </div>
		                                </div>

                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
 @endsection
 @section('script')
 <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
 <script>tinymce.init({ selector:'textarea' });</script>
 @endsection