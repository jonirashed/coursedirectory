 @extends('superadminPanel.master')
 
 @section('mainContent')
 <div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Manage Awarding Body</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                            	@if(Session::has('success'))
					                <p class="alert alert-success">{{ Session::get('success') }}</p>
					            @elseif(Session::has('danger'))
					                <p class="alert alert-danger">{{ Session::get('danger') }}</p>
					            @endif
                                    <div class="col-lg-12">
					                        <div class="panel-body">
					                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
					                                <thead>
					                                    <tr>
					                                        <th>SL</th>
					                                        <th>Name</th>
					                                        <th>Slug</th>
					                                        <th>Status</th>
					                                        <th>Action</th>
					                                    </tr>
					                                </thead>
					                                <tbody>
					                                	@php $i = 1; @endphp
					                                	@foreach($awardingBodies as $awardingBody)
					                                	
					                                    <tr class="odd gradeX">
					                                        <td>{{$i++}}</td>
					                                        <td>{{$awardingBody->name}}</td>
					                                        <td>{{$awardingBody->slug}}</td>
					                                        <td>@if($awardingBody->status == 1)
					                                        	 <span class="btn btn-success btn-xs">Published</span> 
					                                        	@else
					                                        	 <span class="btn btn-warning btn-xs">Unpublished</span>
					                                        	@endif
					                                        </td>
					                                        <td><a href="{{url('/superAdminBangla1desh/awarding/body/'.$awardingBody->id.'/edit/')}}" class="btn btn-primary btn-xs">Edit</a>
					                                        	<a href="{{url('/superAdminBangla1desh/awarding/body/delete/'.$awardingBody->id)}}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
					                                        </td>
					                                    </tr>
					                                    @endforeach
					                                </tbody>
					                            </table>
					                        </div>
					                        <!-- /.panel-body -->
					                </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
 @endsection
 @section('script')
 <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
 @endsection