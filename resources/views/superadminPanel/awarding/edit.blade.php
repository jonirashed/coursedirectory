 @extends('superadminPanel.master')
 
 @section('mainContent')
 <div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Awarding Body</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-8">
                                    <form action="{{url('superAdminBangla1desh/awarding/body/'.$awardingBody->id)}}" method="POST">
                                    	{{csrf_field()}}
                                    	{{ method_field('PATCH') }}
                                    	<div class="form-group mrgn {{ $errors->has('name') ? 'has-error' : '' }}">
	                                    	<div class="col-sm-4">
		                                    	<label>Name</label>
		                                    </div>
		                                    <div class="col-sm-8 ">
		                                        <input type="hidden" name="id" class="form-control mrgn_btn" value="{{ $awardingBody->name }}" value="{{$awardingBody->id}}">
		                                        <input type="text" name="name" class="form-control mrgn_btn" value="{{ $awardingBody->name }}">
		                                        @if($errors->has('name'))
		                                            <span class="text-danger">{{ $errors->first('name') }}</span>
		                                        @endif
		                                    </div>
		                                </div>
		                                <div class="form-group">
	                                    	<div class="col-sm-4">
		                                    	<label>Slug</label>
		                                    </div>
		                                    <div class="col-sm-8">
		                                        <input type="text" name="slug" class="form-control mrgn_btn" value="{{$awardingBody->slug}}">
		                                    </div>
		                                </div>
                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                            <div class="col-sm-4">
		                                    	<label>Publication Status</label>
		                                    </div>
		                                    <div class="col-sm-8">
	                                            <select class="form-control mrgn_btn" name="status">
	                                                <option value="">Select Status</option>
	                                                <option {{$awardingBody->status == 1?'selected': ''}} value="1">Publish</option>
	                                                <option {{$awardingBody->status == 2?'selected': ''}} value="2">Unpublish</option>
	                                            </select>
	                                            @if($errors->has('status'))
		                                            <span class="text-danger">{{ $errors->first('status') }}</span>
		                                        @endif
	                                        </div>
                                        </div>
		                                <div class="form-group">
		                                    <div class="col-sm-2 pull-right">
		                                        <button type="submit" class="btn btn-primary">Update</button>
		                                    </div>
		                                </div>

                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
 @endsection
 @section('script')
 <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
 <script>tinymce.init({ selector:'textarea' });</script>
 @endsection