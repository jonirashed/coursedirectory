
// Course Details
$(document).ready(function(){
    $("#courseDetailsNext").click(function(event){
    	event.preventDefault();
        $('#courseDetails').hide();
        $('#qualification').show();
    });
});

// Qualification

$(document).ready(function(){
    $("#qualificationNext").click(function(event){
    	event.preventDefault();
        $('#pricing').show();
        $('#qualification').hide();
    });
});

$(document).ready(function(){
    $("#qualificationPrevious").click(function(event){
    	event.preventDefault();
        $('#courseDetails').show();
        $('#qualification').hide();
    });
});

// Pricing

$(document).ready(function(){
    $("#pricingNext").click(function(event){
    	event.preventDefault();
        $('#content').show();
        $('#pricing').hide();
    });
});

$(document).ready(function(){
    $("#pricingPrevious").click(function(event){
    	event.preventDefault();
        $('#qualification').show();
        $('#pricing').hide();
    });
});


// Content

$(document).ready(function(){
    $("#contentNext").click(function(event){
    	event.preventDefault();
        $('#extras').show();
        $('#content').hide();
    });
});

$(document).ready(function(){
    $("#contentPrevious").click(function(event){
    	event.preventDefault();
        $('#pricing').show();
        $('#content').hide();
    });
});

// Extras

$(document).ready(function(){
    $("#contentPrevious").click(function(event){
    	event.preventDefault();
        $('#pricing').show();
        $('#content').hide();
    });
});