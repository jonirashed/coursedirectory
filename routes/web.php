<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// SuperAdmin Routes start

Route::prefix('superAdminBangla1desh')->group(function () {
	// Course Category
	Route::get('/', 'SuperadminController@index');
	Route::get('/category/create/', 'CategoryController@create');
	Route::post('/category/store/', 'CategoryController@store');
	Route::get('/category/manage/', 'CategoryController@manage');
	Route::get('/category/delete/{id}', 'CategoryController@delete');
	Route::get('/category/edit/{id}', 'CategoryController@edit');
	Route::post('/categoiry/update/', 'CategoryController@update');

	

	// Awarding Body
	Route::resource('awarding/body', 'AwardingBodyController');
	Route::get('/awarding/body/delete/{id}', 'AwardingBodyController@deleteById');
	// Qualification
	Route::resource('qualification', 'QualificationController');
	Route::get('/qualification/delete/{id}', 'QualificationController@deleteById');
});

// Front End Routes
Route::get('/', 'PublicController@home');


// E-mail Varification
Route::get('/cd_users/confirmation/{token}', 'AuthController@confirmation')->name('confirmation');

// User Auth Routes
Route::group(['middleware' => ['CheckUserMiddleware']], function () {

	Route::get('/registration', 'AuthController@registration');
	Route::get('/login', 'AuthController@login');
	Route::post('/user/store', 'AuthController@userStore');
	Route::post('/user/signIn', 'AuthController@signIn');

});

// user dashboard
Route::group(['middleware' => ['UserDashboardMiddleware']], function () {

	Route::post('/user/logout', 'AuthController@userLogout');
	Route::get('/user/dashboard', 'AuthController@userDashboard');

	Route::prefix('/seller/panel/')->group(function () {
		// Course Category
		Route::get('/course/create', 'CourseController@create');
		Route::post('/course/store', 'CourseController@courseStore');
		Route::get('/course/manage', 'CourseController@courseManage');

	});	

});


// Seller Routes




