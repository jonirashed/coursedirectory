<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CdQualification extends Model
{
    protected $fillable = ['name', 'status'];
}
