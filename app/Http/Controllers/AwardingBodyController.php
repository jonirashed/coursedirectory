<?php

namespace App\Http\Controllers;
use App\CourseAwardingBody;
use Illuminate\Http\Request;

class AwardingBodyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $awardingBodies = CourseAwardingBody::all();
        return view('superadminPanel.awarding.manage')->with('awardingBodies', $awardingBodies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('superadminPanel.awarding.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'status' => 'required'
        ]);
        $awardingBody = new CourseAwardingBody();
        $awardingBody->name = $request->name;
        $awardingBody->slug = $request->slug;
        $awardingBody->status = $request->status;
        $save = $awardingBody->save();
        if($save){
            return redirect()->back()->with('success', 'Success!! Awarding Body Created Successfully.');
        }else{
            return redirect()->back()->with('danger', 'Sorry!! Awarding Body Created Unsccessfully.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $awardingBody = CourseAwardingBody::find($id);
       return view('superadminPanel.awarding.edit')->with('awardingBody', $awardingBody);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'status' => 'required'
        ]);

        $awardingBody = CourseAwardingBody::find($id);
        $awardingBody->name = $request->name;
        $awardingBody->slug = $request->slug;
        $awardingBody->status = $request->status;
        $save = $awardingBody->save();
        if($save){
            return redirect('superAdminBangla1desh/awarding/body/')->with('success', 'Success!! Awarding Body Info Updated Successfully.');
        }else{
            return redirect('superAdminBangla1desh/awarding/body/')->with('danger', 'Sorry!! Awarding Body Info Updated Unsccessfully.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deleteById($id)
    {
        $awardingBody = CourseAwardingBody::destroy($id);
        if($awardingBody){
            return redirect()->back()->with('success', 'Success!! Awarding Body Info Deleted Successfully.');
        }else{
            return redirect()->back()->with('danger', 'Sorry!! Awarding Body Info Deleted Unsccessfully.');
        }
    }
}
