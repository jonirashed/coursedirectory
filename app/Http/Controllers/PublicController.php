<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CdCourse;
class PublicController extends Controller
{
    public function home(){
    	$courses = CdCourse::all();
    	return view('frontEnd.master')->with('courses', $courses);
    }
}
