<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourseCategory;
use App\CdCourse;
class CourseController extends Controller
{
    public function create(){
    	$subjects = CourseCategory::all();
    	return view('frontEnd.dashboard.course.create')->with('subjects', $subjects);
    }

    public function courseStore(Request $request){
    	$course = new CdCourse();
    	$course->title = $request->title;
    	$course->shortDescription = $request->shortDescription;
    	$course->price = $request->price;
    	$course->status = $request->status;
    	$save = $course->save();
    	if($save){
            return redirect()->back()->with('success', 'Success!! Course Created Successfully.');
        }else{
            return redirect()->back()->with('danger', 'Sorry!! Course Created Unsccessfully.');
        }
    }

    public function courseManage(){
    	$courses = CdCourse::all();
    	return view('frontEnd.dashboard.course.manage')->with('courses', $courses);
    }
}
