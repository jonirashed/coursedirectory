<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourseCategory;
class CategoryController extends Controller
{
    public function create(){
    	$categories = CourseCategory::all();
    	return view('superadminPanel.category.create')->with('categories', $categories);
    }
    public function store(Request $request){

        $request->validate([
            'name' => 'required',
            'status' => 'required'
        ]);

        $category = new CourseCategory();
        $category->name = $request->name;
        $category->description = $request->description;
        $category->slug = $request->slug;
        $category->parent_id = empty($request->parent_id)? 0: $request->parent_id;
        $category->status = $request->status;
        $save = $category->save();
        if($save){
            return redirect()->back()->with('success', 'Success!! Category Created Successfully.');
        }else{
            return redirect()->back()->with('danger', 'Sorry!! Category Created Unsccessfully.');
        }
    }
    public function update(Request $request){

    	$request->validate([
	        'name' => 'required',
	        'status' => 'required'
	    ]);

    	$category = CourseCategory::find($request->id);
    	$category->name = $request->name;
    	$category->description = $request->description;
    	$category->slug = $request->slug;
    	$category->parent_id = empty($request->parent_id)? 0: $request->parent_id;
    	$category->status = $request->status;
    	$save = $category->save();
    	if($save){
    		return redirect('superAdminBangla1desh/category/manage')->with('success', 'Success!! Category Updated Successfully.');
    	}else{
    		return redirect('superAdminBangla1desh/category/manage')->with('danger', 'Sorry!! Category Updated Unsccessfully.');
    	}
    }

    public function manage(){
        $categories = CourseCategory::all();
        return view('superadminPanel.category.manage')->with('categories', $categories);
    }

    public function edit($id){
        $categories = CourseCategory::all();
        $category = CourseCategory::find($id);
        return view('superadminPanel.category.edit')->with(['category' => $category, 'categories' => $categories]);
    }

    public function delete($id){
        $category = CourseCategory::destroy($id);
        if($category){
            return redirect()->back()->with('success', 'Success!! Category Deleted Successfully.');
        }else{
            return redirect()->back()->with('danger', 'Sorry!! Category Deleted Unsccessfully.');
        }
    }
}
