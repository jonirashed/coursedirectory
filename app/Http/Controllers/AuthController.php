<?php

namespace App\Http\Controllers;
use App\CdUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Session;
use Mail;
class AuthController extends Controller
{
    public function registration(){
    	return view('frontEnd.auth.registration');
    }

    public function login(){
    	return view('frontEnd.auth.login');
    }

    public function signIn(Request $request){
    	$request->validate([
    		'email' => 'required',
    		'password' => 'required',
    	]);

    	$user = CdUser::select('*')->where([['email', $request->email], ['approved', 1]])->first();

    	if(count($user) > 0){
    		$password = Crypt::decryptString($user->password);
	    	if($password == $request->password){
	    		session(['userType' => $user->userType]);
	    		return redirect('user/dashboard');
	    	}else{
	    		return redirect()->back()->with('danger', 'Sorry!! Varify your E-mail Or E-mail Or Password is invalid');
	    	}
    	}else{
    		return redirect()->back()->with('danger', 'Sorry!! Varify your E-mail Or E-mail Or Password is invalid');
    	}
    }

    public function userLogout(){
    	session(['userType' => '']);
    	return redirect('/login');
    }

    public function userDashboard(){
    	return view('frontEnd.dashboard.master');
    }

    protected function create(array $data)
    {
        return CdUser::create([
            'fName' => $data['fName'],
            'lName' => $data['lName'],
            'phone' => $data['phone'],
            'userType' => $data['userType'],
            'companyName' => $data['companyName'],
            'email' => $data['email'],
            'password' => Crypt::encryptString($data['password']),
        ]);
    }

    public function userStore(Request $request){
    	$request->validate([
    		'fName' => 'required',
    		'lName' => 'required',
    		'phone' => 'required',
    		'userType' => 'required',
    		'email' => 'unique:cd_users,email|required',
    		'password' => 'required',
    	]);

        $input = $request->all();


        $data = $this->create($input)->toArray();

        $data['token'] = str_random(25);

        $user = CdUser::find($data['id']);
        $user->token = $data['token'];
        $user->save();

            Mail::send('frontEnd.mails.confirmation',  $data, function($message) use($data){
                $message->to($data['email']);
                $message->subject('Registration Confirmation');
            });

            return redirect('login')->with('success', 'Success!! Confirmation email has been sent, pls check your email');

    }

    public function confirmation($token){
        $user = CdUser::where('token', $token)->first();

        if(!empty($user)){
            $user->approved = 1;
            $user->token = '';
            $user->save();
            return redirect('login')->with('success', 'Your activation is completed');
        }
        return redirect('login')->with('danger', 'Something went wrong');
    }
}
