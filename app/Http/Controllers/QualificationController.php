<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CdQualification;
class QualificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $qualifications = CdQualification::all();
        return view('superadminPanel.qualification.manage')->with('qualifications', $qualifications);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('superadminPanel.qualification.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'status' => 'required'
        ]);

        $cdQualification = new CdQualification();
        $cdQualification->name = $request->name;
        $cdQualification->slug = $request->slug;
        $cdQualification->status = $request->status;
        $save = $cdQualification->save();
        if($save){
            return redirect()->back()->with('success', 'Success!! Qualification Created Successfully.');
        }else{
            return redirect()->back()->with('danger', 'Sorry!! Qualification Created Unsccessfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $qualification = CdQualification::find($id);
       return view('superadminPanel.qualification.edit')->with('qualification', $qualification);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'status' => 'required'
        ]);

        $CdQualification = CdQualification::find($id);
        $CdQualification->name = $request->name;
        $CdQualification->slug = $request->slug;
        $CdQualification->status = $request->status;
        $save = $CdQualification->save();
        if($save){
            return redirect('superAdminBangla1desh/qualification/')->with('success', 'Success!! Qualification Info Updated Successfully.');
        }else{
            return redirect('superAdminBangla1desh/qualification/')->with('danger', 'Sorry!! Qualification Info Updated Unsccessfully.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deleteById($id)
    {
        $CdQualification = CdQualification::destroy($id);
        if($CdQualification){
            return redirect()->back()->with('success', 'Success!! Qualification Info Deleted Successfully.');
        }else{
            return redirect()->back()->with('danger', 'Sorry!! Qualification Info Deleted Unsccessfully.');
        }
    }
}
