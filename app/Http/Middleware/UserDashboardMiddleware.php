<?php

namespace App\Http\Middleware;

use Closure;
use Session;
class UserDashboardMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        session_start();
        $userType = Session::get('userType');
        if ($userType == 'seller' || $userType == 'buyer') {
            return $next($request);
        }
        return redirect('/login');
    }
}
