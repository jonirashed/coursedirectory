<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class CheckUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        session_start();
        $userType = Session::get('userType');
        if ($userType == '') {
            return $next($request);
        }
        return redirect('/user/dashboard');
        
    }
}
