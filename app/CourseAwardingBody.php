<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseAwardingBody extends Model
{
    protected $fillable = ['name', 'status'];
}
