<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CdUser extends Model
{
    protected $fillable = ['fName', 'lName', 'phone', 'userType', 'email', 'password'];
}
