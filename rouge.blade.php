<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{Session::get('userType')}} Panel</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('/public/superadminPanel/')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{asset('/public/superadminPanel/')}}/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{asset('/public/superadminPanel/')}}/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{asset('/public/superadminPanel/')}}/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{asset('/public/superadminPanel/')}}/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">{{Session::get('userType')}} Panel</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 1</strong>
                                        <span class="pull-right text-muted">40% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 2</strong>
                                        <span class="pull-right text-muted">20% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 3</strong>
                                        <span class="pull-right text-muted">60% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 4</strong>
                                        <span class="pull-right text-muted">80% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Course<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{url('/seller/panel/course/create')}}">Add Course</a>
                                </li>
                                <li>
                                    <a href="morris.html">Morris.js Charts</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Flot Charts</a>
                                </li>
                                <li>
                                    <a href="morris.html">Morris.js Charts</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="tables.html"><i class="fa fa-table fa-fw"></i> Tables</a>
                        </li>
                        <li>
                            <a href="forms.html"><i class="fa fa-edit fa-fw"></i> Forms</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="panels-wells.html">Panels and Wells</a>
                                </li>
                                <li>
                                    <a href="buttons.html">Buttons</a>
                                </li>
                                <li>
                                    <a href="notifications.html">Notifications</a>
                                </li>
                                <li>
                                    <a href="typography.html">Typography</a>
                                </li>
                                <li>
                                    <a href="icons.html"> Icons</a>
                                </li>
                                <li>
                                    <a href="grid.html">Grid</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Third Level <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="blank.html">Blank Page</a>
                                </li>
                                <li>
                                    <a href="login.html">Login Page</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Create Course</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                @if(Session::has('success'))
                                    <p class="alert alert-success">{{ Session::get('success') }}</p>
                                @elseif(Session::has('danger'))
                                    <p class="alert alert-danger">{{ Session::get('danger') }}</p>
                                @endif
                                <div class="col-lg-8">
                                    <form role="form" action="{{url('/seller/panel/course/store/')}}" method="post">
                                        {{csrf_field()}}

                                        <!-- Course Details -->
                                        <div id="courseDetails">
                                            <div class="row">
                                                <div class="col-lg-6 col-lg-offset-4" style="margin-bottom: 15px;">
                                                    <h2>Course Details</h2>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label class="mrgn_tp">Title<span style="color: red; padding: 3px;">*</span></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="courseTitle" class="form-control mrgn_btn mrgn_tp" placeholder="Slug">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label>Subject (Maximum of 3)<span style="color: red; padding: 3px;">*</span></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <select class="form-control mrgn_btn" name="status">
                                                            <option value="">Select</option>
                                                            @foreach($subjects as $subject)
                                                            <option value="1">Publish</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                 <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label>Study method<span style="color: red; padding: 3px;">*</span></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p>How is the course content delivered to students?</p>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <input type="radio" name="studyMethod"><span>  Online</span>
                                                                <div class="methodCategory">
                                                                    <div class="col-lg-12">
                                                                        <input type="radio" name="studyMethodCategory"><span> Self-paced</span>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <input type="radio" name="studyMethodCategory"><span> With Live Classes</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <input type="radio" name="studyMethod"><span>  Distance Learning</span>
                                                                <div class="col-lg-12">
                                                                    <input type="radio" name="studyMethodCategory"><span> Self-paced</span>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <input type="radio" name="studyMethodCategory"><span> With Live Classes</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <input type="radio" name="studyMethod"><span>  Classroom</span>
                                                                <div class="col-lg-12">
                                                                    <input type="radio" name="studyMethodCategory"><span> Classroom only</span>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <input type="radio" name="studyMethodCategory"><span> With an online component (blended)</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <input type="radio" name="studyMethod"><span>  On-site / in-company</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label>Study mode</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <input type="checkbox" name="studyMethodCategory"><span> Part-time</span>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <input type="checkbox" name="studyMethodCategory"><span> Full-time</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label>Duration (Optional)</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <input type="text" name="duration" placeholder="e.g 30 hours" class="form-control">
                                                            </div>
                                                            <div class="col-lg-7 pull-right">
                                                                <select class="form-control" name="duration_format">
                                                                    <option value="">Select Fortam</option>
                                                                    <option value="hour">Hour(s)</option>
                                                                    <option value="day">Day(s)</option>
                                                                    <option value="week">Week(s)</option>
                                                                    <option value="wonth">Month(s)</option>
                                                                    <option value="year">Year(s)</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label>Level<span style="color: red; padding: 3px;">*</span></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <input type="checkbox" name="courseLevel"><span> Beginner</span>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <input type="checkbox" name="courseLevel"><span> Intermediate</span>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <input type="checkbox" name="courseLevel"><span> Advanced</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label>Tutor support</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <input type="radio" name="tutor"><span> Yes</span>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <input type="radio" name="tutor"><span> No</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label>Is this course an apprenticeship?</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <input type="radio" name="apprenticeship"><span> Yes</span>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <input type="radio" name="apprenticeship"><span> No</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-4 pull-right text-right">
                                                        <a href="#" class="btn btn-primary" id="courseDetailsNext">Next</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                       <!--  Qualifications -->
                                        <div id="qualification" style="display: none">
                                            <div class="row">
                                                <div class="col-lg-6 col-lg-offset-4" style="margin-bottom: 15px;">
                                                    <h2>Qualifications</h2>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label class="mrgn_tp">Slug</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="qualification" class="form-control mrgn_btn mrgn_tp" placeholder="Slug">
                                                    </div>
                                                </div>
                                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                                    <div class="col-sm-4">
                                                        <label>Publication Status</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <select class="form-control mrgn_btn" name="status">
                                                            <option value="">Select Status</option>
                                                            <option value="1">Publish</option>
                                                            <option value="2">Unpublish</option>
                                                        </select>
                                                        @if($errors->has('status'))
                                                            <span class="text-danger">{{ $errors->first('status') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-4 pull-right text-right">
                                                        <a href="#" class="btn btn-primary" id="qualificationNext">Next</a>
                                                    </div>
                                                    <div class="col-sm-4 pull-left">
                                                        <a href="#" class="btn btn-default" id="qualificationPrevious">Previous</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Pricing -->
                                        <div id="pricing" style="display: none">
                                            <div class="row">
                                                <div class="col-lg-6 col-lg-offset-4" style="margin-bottom: 15px;">
                                                    <h2>Pricing</h2>
                                                </div>
                                               
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label class="mrgn_tp">Slug</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="pricing" class="form-control mrgn_btn mrgn_tp" placeholder="Slug">
                                                    </div>
                                                </div>
                                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                                    <div class="col-sm-4">
                                                        <label>Publication Status</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <select class="form-control mrgn_btn" name="status">
                                                            <option value="">Select Status</option>
                                                            <option value="1">Publish</option>
                                                            <option value="2">Unpublish</option>
                                                        </select>
                                                        @if($errors->has('status'))
                                                            <span class="text-danger">{{ $errors->first('status') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-4 pull-right text-right">
                                                        <a href="#" class="btn btn-primary" id="pricingNext">Next</a>
                                                    </div>
                                                    <div class="col-sm-4 pull-left">
                                                        <a href="#" class="btn btn-default" id="pricingPrevious">Previous</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Content -->
                                        <div id="content" style="display: none">
                                            <div class="row">
                                                <div class="col-lg-6 col-lg-offset-4" style="margin-bottom: 15px;">
                                                    <h2>Content</h2>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label class="mrgn_tp">Slug</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="content" class="form-control mrgn_btn mrgn_tp" placeholder="Slug">
                                                    </div>
                                                </div>
                                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                                    <div class="col-sm-4">
                                                        <label>Publication Status</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <select class="form-control mrgn_btn" name="status">
                                                            <option value="">Select Status</option>
                                                            <option value="1">Publish</option>
                                                            <option value="2">Unpublish</option>
                                                        </select>
                                                        @if($errors->has('status'))
                                                            <span class="text-danger">{{ $errors->first('status') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-4 pull-right text-right">
                                                        <a href="#" class="btn btn-primary" id="contentNext">Next</a>
                                                    </div>
                                                    <div class="col-sm-4 pull-left">
                                                        <a href="#" class="btn btn-default" id="contentPrevious">Previous</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Extras -->
                                        <div id="extras" style="display: none">
                                            <div class="row">
                                                <div class="col-lg-6 col-lg-offset-4" style="margin-bottom: 15px;">
                                                    <h2>Extras</h2>
                                                </div>
                                               
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label class="mrgn_tp">Slug</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="extras" class="form-control mrgn_btn mrgn_tp" placeholder="Slug">
                                                    </div>
                                                </div>
                                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                                    <div class="col-sm-4">
                                                        <label>Publication Status</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <select class="form-control mrgn_btn" name="status">
                                                            <option value="">Select Status</option>
                                                            <option value="1">Publish</option>
                                                            <option value="2">Unpublish</option>
                                                        </select>
                                                        @if($errors->has('status'))
                                                            <span class="text-danger">{{ $errors->first('status') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-4 text-left">
                                                        <a href="#" class="btn btn-default" id="extrasPrevious">Previous</a>
                                                    </div>
                                                    <div class="col-sm-4 pull-right text-right">
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{asset('/public/superadminPanel/')}}/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('/public/superadminPanel/')}}/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{asset('/public/superadminPanel/')}}/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{asset('/public/superadminPanel/')}}/vendor/raphael/raphael.min.js"></script>
    <script src="{{asset('/public/superadminPanel/')}}/vendor/morrisjs/morris.min.js"></script>
    <script src="{{asset('/public/superadminPanel/')}}/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{asset('/public/superadminPanel/')}}/dist/js/sb-admin-2.js"></script>
    <!-- custom js -->
    <script src="{{asset('/public/frontEnd/')}}/js/custom.js"></script>

</body>

</html>
